#!/usr/bin/env python

import conf
import signal
from websocket import create_connection
import json
import sys
import xlwings as xw
import threading

api_status = {"ping"}
api_public = {"trade", "book", "ticker", "spread", "ohlc"}
api_private = {"openOrders", "ownTrades", "balances"}
api_trading = {"addOrder", "cancelOrder", "cancelAll", "cancelAllOrdersAfter"}
api_domain_public = "wss://ws.kraken.com/"
api_futures_domain_public = 'wss://futures.kraken.com/ws/v1'
api_domain_private = "wss://ws-auth.kraken.com/"
api_symbols = ""
api_number = 0


class KrakenPublisher:

    def __init__(self, currency_pairs=conf.KRAKEN_SPOT_CURRENCY_PAIRS, futures=False, future_pairs=conf.KRAKEN_FUTURE_CONTRACTS):
        self.currency_pairs = currency_pairs
        self.future_currency_pairs = future_pairs
        self.futures = futures

    def get_api_data(self, api_feed):

        if self.futures:
            return api_futures_domain_public, '{"event":"subscribe", "feed":"%(feed)s", "product_ids":["%(symbols)s"]}' % {"feed":api_feed, "symbols":'","'.join(self.future_currency_pairs)}

        if api_feed in api_status:
            api_domain = api_domain_public
            api_data = '{"event":"%(feed)s"}' % {"feed":api_feed}
            signal.alarm(3)
        elif api_feed in api_public:
            api_symbols = '","'.join(self.currency_pairs)
            if api_feed == 'book':
                api_data = '{"event":"subscribe", "subscription":{"name":"%(feed)s", "depth":%(depth)d}, "pair":["%(symbols)s"]}' % {"feed":api_feed, "symbols":api_symbols, "depth":api_number if api_number != 0 else 10}
            elif api_feed == 'ohlc':
                api_data = '{"event":"subscribe", "subscription":{"name":"%(feed)s", "interval":%(interval)d}, "pair":["%(symbols)s"]}' % {"feed":api_feed, "symbols":api_symbols, "interval":api_number if api_number != 0 else 1}
            else:
                api_data = '{"event":"subscribe", "subscription":{"name":"%(feed)s"}, "pair":["%(symbols)s"]}' % {"feed":api_feed, "symbols":api_symbols}
            api_domain = api_domain_public
        elif api_feed in api_private:
            api_domain = api_domain_private
            try:
                api_token = open("WS_Token").read().strip()
            except:
                print("WebSocket authentication token missing (WS_Token)")
                sys.exit(1)
            if api_feed == 'openOrders':
                api_data = '{"event":"subscribe", "subscription":{"name":"%(feed)s", "ratecounter":%(ratecounter)s, "token":"%(token)s"}}' % {"feed":api_feed, "ratecounter":sys.argv[2].split('=')[1], "token":api_token}
            elif api_feed == 'ownTrades':
                api_data = '{"event":"subscribe", "subscription":{"name":"%(feed)s", "snapshot":%(snapshot)s, "token":"%(token)s"}}' % {"feed":api_feed, "snapshot":sys.argv[2].split('=')[1], "token":api_token}
            else:
                api_data = '{"event":"subscribe", "subscription":{"name":"%(feed)s", "token":"%(token)s"}}' % {"feed":api_feed, "token":api_token}
        elif api_feed in api_trading:
            api_domain = api_domain_private
            try:
                api_token = open("WS_Token").read().strip()
            except:
                print("WebSocket authentication token missing (WS_Token)")
                sys.exit(1)
            api_data = '{"event":"%(feed)s", "token":"%(token)s"' % {"feed":api_feed, "token":api_token}
            for count in range(2, len(sys.argv)):
                if sys.argv[count].split('=')[0] == 'txid':
                    api_data = api_data + ', "%(name)s":["%(value)s"]' % {"name":sys.argv[count].split('=')[0], "value":sys.argv[count].split('=')[1].replace(',', '","')}
                elif sys.argv[count].split('=')[0] == 'reqid':
                    api_data = api_data + ', "%(name)s":%(value)s' % {"name":sys.argv[count].split('=')[0], "value":sys.argv[count].split('=')[1]}
                elif sys.argv[count].split('=')[0] == 'timeout':
                    api_data = api_data + ', "%(name)s":%(value)s' % {"name":sys.argv[count].split('=')[0], "value":sys.argv[count].split('=')[1]}
                else:
                    api_data = api_data + ', "%(name)s":"%(value)s"' % {"name":sys.argv[count].split('=')[0], "value":sys.argv[count].split('=')[1]}
            api_data = api_data + '}'
            signal.alarm(3)
        else:
            print("Usage: %s feed/endpoint [parameters]" % sys.argv[0])
            print("Example: %s ticker XBT/USD" % sys.argv[0])
            sys.exit(1)

        return api_domain, api_data

    def connect(self, api_domain, ):

        try:
            self.ws = create_connection(api_domain)
            print("WebSocket -> Client: %s" % self.ws.recv())
        except Exception as error:
            print("WebSocket connection failed (%s)" % error)
            sys.exit(1)

    def start(self,api_data):
        try:
            print("Client -> WebSocket: %s" % api_data)
            self.ws.send(api_data)
            print("WebSocket -> Client: %s" % self.ws.recv())
        except Exception as error:
            print("WebSocket subscription/request failed (%s)" % error)
            self.ws.close()
            sys.exit(1)

        while True:
            try:
                message = self.ws.recv()
                message = json.loads(message)
                #print("WebSocket -> Client: %s" % message)
                self.process_price_message(message)

            except KeyboardInterrupt:
                self.ws.close()
                sys.exit(0)


    def process_price_message(self, msg):

        wb = xw.Book('market_data.xlsx')
        sht1 = wb.sheets['live_data']

        if self.futures and 'event' not in msg.keys(): #Then it is futures
            symbol = msg['product_id']
            bid0 = msg['bid']
            ask0 = msg['ask']

            sht1.range(f'H{conf.KRAKEN_FUTURE_CONTRACTS.index(symbol) + 2}').value = symbol
            sht1.range(f'F{conf.KRAKEN_FUTURE_CONTRACTS.index(symbol) + 2}').value = bid0
            sht1.range(f'G{conf.KRAKEN_FUTURE_CONTRACTS.index(symbol) + 2}').value = ask0

            #    {'exchange': self.exchange, 'symbol': msg['product_id'][3:].lower(), 'timestamp': time.time(),
            #     'bids': [[msg['bid'],msg['bid_size']]], 'asks': [[msg['ask'], msg['ask_size']]]}, market='futures')
        else:

            if type(msg) == dict:  # heartbeat
                return

            symbol = msg[-1]
            bid0 = msg[1]['b'][0]
            ask0 = msg[1]['a'][0]
            close = msg[1]['c'][0]
            sht1.range(f'A{conf.KRAKEN_SPOT_CURRENCY_PAIRS.index(symbol) + 2}').value = symbol
            sht1.range(f'B{conf.KRAKEN_SPOT_CURRENCY_PAIRS.index(symbol) + 2}').value = bid0
            sht1.range(f'C{conf.KRAKEN_SPOT_CURRENCY_PAIRS.index(symbol) + 2}').value = ask0
            sht1.range(f'D{conf.KRAKEN_SPOT_CURRENCY_PAIRS.index(symbol) + 2}').value = close

            #print('SPOT: ', symbol, bid0, ask0, msg[1]['c'][0])

            #    {'exchange': self.exchange, 'symbol': msg[-1].replace('/','').lower(), 'timestamp': time.time(),
            # 'bids': [msg[1]['b']], 'asks': [msg[1]['a']]}, market='spot')

        return


def start_streaming(futures):
    bp = KrakenPublisher(futures=futures)
    api_domain, api_data = bp.get_api_data('ticker')
    bp.connect(api_domain)
    bp.start(api_data)


if __name__=='__main__':

    wb = xw.Book('market_data.xlsx')

    t_spot = threading.Thread(target=start_streaming, args=(False,))
    t_spot.start()
    t_futures = threading.Thread(target=start_streaming, args=(True,))
    t_futures.start()

